# PetTagsNgrx

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.6.

## Demo

LINK: https://pet-tag-ngrx.firebaseapp.com

![alt text](https://firebasestorage.googleapis.com/v0/b/pet-tag-ngrx.appspot.com/o/demo1.jpg?alt=media&token=f9011022-279c-41c4-b017-efa638640503)

![alt text](https://firebasestorage.googleapis.com/v0/b/pet-tag-ngrx.appspot.com/o/demo2.png?alt=media&token=5473c2e0-87c8-4411-8225-aebb13c25450)

![alt text](https://firebasestorage.googleapis.com/v0/b/pet-tag-ngrx.appspot.com/o/demo3.png?alt=media&token=f3a6cffd-617e-4c42-851e-9529059b8332)



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
