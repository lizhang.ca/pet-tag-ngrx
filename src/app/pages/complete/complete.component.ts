// src/app/pages/complete/complete.component.ts
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { RESET } from './../../core/pet-tag.actions';
import { PetTag } from './../../core/pet-tag.model';

@Component({
  selector: 'app-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.css']
})
export class CompleteComponent implements OnInit, OnDestroy {
  dog = '';
  dis = 'dog5-0';
  tagState$: Observable<PetTag>;
  private tagStateSubscription: Subscription;
  petTag: PetTag;

  constructor(private store: Store<PetTag>) {
    this.tagState$ = store.select('petTag');
  }

  ngOnInit() {
    this.tagStateSubscription = this.tagState$.subscribe((state) => {
      this.petTag = state;
      console.log(state);
    });
  }

  ngOnDestroy() {
    this.tagStateSubscription.unsubscribe();
  }

  newTag() {
    this.store.dispatch({
      type: RESET
    });
  }

  fetchTag() {
    this.dog = 'dog';
    setTimeout(() => { this.dog = ''; }, 10000);

    // setTimeout(() => { this.dis = 'dog5-0'; }, 10000);
    // for (let i = 0; i < 200; i++) {
    //   setTimeout(() => {
    //     this.dis = 'dog5-' + (i % 16) ;
    //   }, 50 * i);
    // }

    let i = 0;
    const fetch = setInterval(() => {
      this.dis = 'dog5-' + (i % 16) ;
      i++;
      if ( i === 200 ) {
        clearInterval(fetch);
      }
    }, 50);


  }

}
