// src/app/core/pet-tag.actions.ts
// export const SELECT_SHAPE = 'SELECT_SHAPE';
// export const SELECT_FONT = 'SELECT_FONT';
// export const ADD_TEXT = 'ADD_TEXT';
// export const TOGGLE_CLIP = 'TOGGLE_CLIP';
// export const TOGGLE_GEMS = 'TOGGLE_GEMS';
// export const COMPLETE = 'COMPLETE';
// export const RESET = 'RESET';


// Section 1
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { PetTag, initialTag } from './pet-tag.model';

// Section 2
export const SELECT_SHAPE = 'SELECT_SHAPE';
export const SELECT_FONT = 'SELECT_FONT';
export const ADD_TEXT = 'ADD_TEXT';
export const TOGGLE_CLIP = 'TOGGLE_CLIP';
export const TOGGLE_GEMS = 'TOGGLE_GEMS';
export const COMPLETE = 'COMPLETE';
export const RESET = 'RESET';

// Section 3
export class SelectShape implements Action {
  readonly type = SELECT_SHAPE;

  constructor(public payload: PetTag) {}
}
export class SelectFont implements Action {
  readonly type = SELECT_FONT;

  constructor(public payload: PetTag) {}
}
export class AddText implements Action {
    readonly type = ADD_TEXT;

    constructor(public payload: PetTag) {}
}
export class ToggleClip implements Action {
  readonly type = TOGGLE_CLIP;

  constructor(public payload: PetTag) {}
}
export class ToggleGems implements Action {
  readonly type = TOGGLE_GEMS;

  constructor(public payload: PetTag) {}
}
export class Complete implements Action {
  readonly type = COMPLETE;

  constructor(public payload: PetTag) {}
}
export class Reset implements Action {
  readonly type = RESET;

  constructor(public payload: PetTag) {}
}


// Section 4
export type Actions = SelectShape | SelectFont | AddText | ToggleClip | ToggleGems | Complete | Reset;
