// src/app/core/pet-tag.reducer.ts
import { Action } from '@ngrx/store';
import { PetTag, initialTag } from './../core/pet-tag.model';
import * as PetTagActions from './pet-tag.actions';

export function petTagReducer(state: PetTag = initialTag, action: PetTagActions.Actions) {
  switch (action.type) {
    case PetTagActions.SELECT_SHAPE:
      return {...state, shape: action.payload};
    case PetTagActions.SELECT_FONT:
      return {...state, font: action.payload};
    case PetTagActions.ADD_TEXT:
      return {...state, text: action.payload};
    case PetTagActions.TOGGLE_CLIP:
      return {...state, clip: !state.clip};
    case PetTagActions.TOGGLE_GEMS:
      return {...state, gems: !state.gems};
    case PetTagActions.RESET:
      return {...state, initialTag};
    case PetTagActions.COMPLETE:
      return {...state, complete: action.payload};
    // case SELECT_FONT:
    //   return Object.assign({}, state, {
    //     font: action.payload
    //   });
    // case ADD_TEXT:
    //   return Object.assign({}, state, {
    //     text: action.payload
    //   });
    // case TOGGLE_CLIP:
    //   return Object.assign({}, state, {
    //     clip: !state.clip
    //   });
    // case TOGGLE_GEMS:
    //   return Object.assign({}, state, {
    //     gems: !state.gems
    //   });
    // case COMPLETE:
    //   return Object.assign({}, state, {
    //     complete: action.payload
    //   });
    // case RESET:
    //   return Object.assign({}, state, initialTag);
    default:
      return state;
  }
}
